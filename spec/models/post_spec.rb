describe Post, type: :model do
  it { expect(build(:post)).to be_valid }

  context 'associations' do
    it { should belong_to :user}
  end

  context 'validations' do
    before do
      create :post
    end
    it { should validate_presence_of :code}
    it { should validate_uniqueness_of(:code).scoped_to(:user_id).case_insensitive }
  end

  describe '.new_thread_code' do
    let(:user) {create :user}
    before do
      user.posts.destroy_all
    end

    context "when there is no thread" do
      let(:thread) {nil}

      it "returns new thread code '1'" do
        expect(Post.new_thread_code(user, thread)).to eql('1')
      end

    end

    context "when code is '1'" do
      let(:thread) {Post.last}

      before do
        create :post, code: '1', user: user
      end

      it "returns new thread code '1.1'" do
        expect(Post.new_thread_code(user, thread)).to eql('1.1')
      end

    end

    context "when code is '10'" do
      let(:thread) {Post.last}

      before do
        create :post, code: '10', user: user
      end

      it "returns new thread code '10.1'" do
        expect(Post.new_thread_code(user, thread)).to eql('10.1')
      end

    end

    context "when code is '1.1'" do
      let(:thread) {Post.last}

      before do
        create :post, code: '1.1', user: user
      end

      it "returns new thread code '1.1.1'" do
        expect(Post.new_thread_code(user, thread)).to eql('1.1.1')
      end

    end

    context "when code is '1.9'" do
      let(:thread) {Post.last}

      before do
        create :post, code: '1.9', user: user
      end

      it "returns new thread code '1.9.1'" do
        expect(Post.new_thread_code(user, thread)).to eql('1.9.1')
      end

    end

    context "when code is '2.1'" do
      let(:thread) {Post.last}

      before do
        create :post, code: '2.1', user: user
      end

      it "returns new thread code '2.1.1'" do
        expect(Post.new_thread_code(user, thread)).to eql('2.1.1')
      end

    end
1
    context "when code is '1.1.1.10'" do
      let(:thread) {Post.last}

      before do
        create :post, code: '1.1.1.10', user: user
      end

      it "returns new thread code '1.1.1.10.1'" do
        expect(Post.new_thread_code(user, thread)).to eql('1.1.1.10.1')
      end

    end
1
  end

  describe '#is_zero_level?' do
    subject {Post.last.is_zero_level?}

    context 'when is zero level' do

      context 'and code is 1' do
        before do
          create :post, code: '1'
        end

        it 'returns zero level true' do
          expect(subject).to be_truthy
        end

      end

      context 'and code is 2' do
        before do
          create :post, code: '2'
        end

        it 'returns zero level true' do
          expect(subject).to be_truthy
        end

      end

    end

    context 'when is not zero level' do
      before do
        create :post, code: '1.1'
      end

      it 'returns zero level false' do
        expect(subject).to be_falsy
      end

    end

  end

  describe '#set_code_group' do
    subject {Post.last.code_group}

    context 'when code is 1' do
      before do
        create :post, code: '1'
      end

      it 'returns code group 001' do
        expect(subject).to eql('001')
      end

    end

    context 'when code is 2' do
      before do
        create :post, code: '2'
      end

      it 'returns code group 002' do
        expect(subject).to eql('002')
      end

    end

    context 'when code is 1.1' do
      before do
        create :post, code: '1.1'
      end

      it 'returns level code 001' do
        expect(subject).to eql('001')
      end

    end

    context 'when code is 1.1.1.1' do
      before do
        create :post, code: '1.1.1.1'
      end

      it 'returns level code 001' do
        expect(subject).to eql('001')
      end

    end

  end

  describe '#set_code_level' do
    subject {Post.last.code_level}

    context 'when code is 1' do
      before do
        create :post, code: '1'
      end

      it 'returns code level 0' do
        expect(subject).to eql(0)
      end

    end

    context 'when code is 1.1' do
      before do
        create :post, code: '1.1'
      end

      it 'returns level code 1' do
        expect(subject).to eql(1)
      end

    end

    context 'when code is 1.1.1.1' do
      before do
        create :post, code: '1.1.1.1'
      end

      it 'returns code level 3' do
        expect(subject).to eql(3)
      end

    end

  end

  describe '#create_virtual_code' do
    subject {Post.last.virtual_code}

    context 'when code is 1' do
      before do
        create :post, code: '1'
      end

      it 'returns 001' do
        expect(subject).to eql('001')
      end

    end

    context 'when code is 001' do
      before do
        create :post, code: '001'
      end

      it 'returns 001' do
        expect(subject).to eql('001')
      end

    end

    context 'when code is 1.1' do
      before do
        create :post, code: '1.1'
      end

      it 'returns 001.001' do
        expect(subject).to eql('001.001')
      end

    end

    context 'when code is 001.001' do
      before do
        create :post, code: '001.001'
      end

      it 'returns 001.001' do
        expect(subject).to eql('001.001')
      end

    end

    context 'when code is 1.1.1' do
      before do
        create :post, code: '1.1.1'
      end

      it 'returns 001.001.001' do
        expect(subject).to eql('001.001.001')
      end

    end

    context 'when code is 001.001.1' do
      before do
        create :post, code: '001.001.1'
      end

      it 'returns 001.001.001' do
        expect(subject).to eql('001.001.001')
      end

    end

  end

end
