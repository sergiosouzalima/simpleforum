describe User, type: :model do
  it { expect(build(:user)).to be_valid }

  context 'associations' do
    it { should have_many :posts }
  end

  context 'validations' do
    it { should validate_presence_of :email }
    it { should validate_uniqueness_of(:email).case_insensitive }
  end

end
