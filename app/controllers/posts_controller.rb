class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  def index
    if params[:post_id].blank?
      @first_index_page = true
      @posts = Post.where( user: current_user ).zero_level.by_virtual_code.page params[:page]
      new_code = Post.new_thread_code( current_user, nil )
      @post = Post.new
      @post.code = new_code
    else
      @first_index_page = false
      code_group = Post.find(params[:post_id]).code_group
      @posts = Post.where( user: current_user ).where( code_group: code_group ).by_virtual_code.page params[:page]
    end
  end

  def show
  end

  def new
    thread = Post.find(params[:id])
    new_code = Post.new_thread_code( current_user, thread )
    @post = Post.new
    @post.code = new_code
  end

  def edit
  end

  def create
    @post = Post.new(post_params)

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: 'Thread was successfully created.' }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: 'Thread was successfully updated.' }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: 'Thread was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @post = Post.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def post_params
    params.require(:post).permit(:code, :virtual_code, :user_id, :message)
  end
end
