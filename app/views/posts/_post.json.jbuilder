json.extract! post, :id, :code, :virtual_code, :user_id, :created_at, :updated_at
json.url post_url(post, format: :json)