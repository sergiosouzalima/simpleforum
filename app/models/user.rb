class User < ActiveRecord::Base
  has_many :posts
  enum role: [:user, :vip, :admin]
  after_initialize :set_default_role, :if => :new_record?
  after_create :create_threads
  validates :email, presence: true
  validates_uniqueness_of :email, case_sensitive: false

  def set_default_role
    self.role ||= :user
  end

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable

  def create_threads
    code_array = ['1', '1.1', '1.1.1', '1.1.1.1', '1.1.2', '1.1.2.1', '1.2',
                  '1.3', '1.4', '1.5', '1.6', '1.7', '1.9', '1.10', '1.11', '1.12',
                  '2','2.1','2.1.1','3','4',
                  '5','6','7','8','9','10','11','12','13','14','15','16',
                  '17','18','19','20','21','22','23','24','25']

    code_array.map do |code|
      post = Post.create( code: code, user: self )
      if post.code == '1'
        message = 'comentarios proibidos: carro arma'
      else
        message = post.is_zero_level? ? "#{post.code.to_i.ordinalize} thread" : "comentário/resposta a thread"
      end
      post.update( message: message )
    end
  end

end
