class Post < ActiveRecord::Base
  belongs_to :user
  validates :code, presence: true
  validates_uniqueness_of :code, scope: :user_id
  after_create :set_attributes, :forbidden_words
  scope :by_virtual_code, -> { order(:virtual_code) }
  scope :zero_level, -> { where("code_level = 0")}

  def self.new_thread_code( user, thread )
    if thread.nil?
      (Post.where( user: user ).zero_level.last.try(:code).to_i + 1).to_s
    else
      unless ( sub_level_posts = Post.where( user: user ).where( code_level: thread.code_level + 1 ).where( super_code: thread.code ) ).blank?
        array_code = sub_level_posts.where( super_code: thread.code ).by_virtual_code.last.code.split('.')
        last_arrray_item = (array_code.pop.to_i + 1).to_s
        (array_code << last_arrray_item).join('.')
      else
        thread.code << '.1'
      end
    end
  end

  def is_zero_level?
    self.code_level == 0
  end

  private

  def forbidden_words
    return true if self.is_zero_level?
    return true if self.message.blank?
    message = Post.where( user: self.user ).where( code_group: self.code_group ).where( code_level: 0 ).first.try(:message)
    return true unless message
    array_menssage = message.split(' ')
    if array_menssage.shift == 'comentarios' && array_menssage.shift == 'proibidos:'
     array_menssage.each do |e|
       self.message.gsub!(e, 'xxxx')
     end
     self.save!
    end
  end

  def set_attributes
    create_virtual_code
    set_code_level
    set_code_group
    set_super_code
    self.save!
  end

  def set_super_code
    if self.is_zero_level?
      self.super_code = '0'
    else
      array_code = self.code.split('.')
      array_code.pop
      self.super_code = array_code.join('.')
    end
  end

  def set_code_group
    self.code_group = self.virtual_code[0,3]
  end

  def set_code_level
    self.code_level = self.virtual_code.count('.')
  end

  def create_virtual_code
    self.virtual_code = self.code.split('.').map do |e|
      e.prepend(zeros_to_prepend(e))
    end.join('.')
  end

  def zeros_to_prepend( str_number )
    return '' if (number = str_number.to_i) >= 100 || str_number.length >= 3
    return '0' if number >= 10
    '00' if number >= 0
  end

end
