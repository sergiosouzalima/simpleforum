module PostsHelper

  def left_spaces(post)
    post.code_level.to_i * 20
  end

  def left_spaces_style(post)
    "display:inline-block; width: #{left_spaces(post)}px;"
  end

end
