Simple Forum App
================

Protótipo simplificado de um fórum de discussão web.

Ruby on Rails
-------------

Esta aplicação foi construída usando:

- Ruby 2.2.2
- Rails 4.2.6

Para saber mais [Installing Rails](http://guides.rubyonrails.org/getting_started.html#installing-rails)

Funcionamento
---------------

Protótipo simplificado de um fórum de discussão web.


A discussão se dá em threads (tópicos), que são comentários encadeados no estilo árvore.

A interface web contempla:

- Uma listagem das threads com paginação, apresentando um formulário para criar
  uma nova thread (figura 1).


- Páginas de exibição de threads (figura 2a). Exibe os comentários em árvore,
  sendo cada  comentário acompanhado de um formulário para resposta (figura 2c).
  Essa página também tem paginação (figura 2b).

- Os tópicos e os comentários são paginados;

- Comentários com palavras presentes em uma blacklist são
rejeitados (figura 3a,b), (figura 3c,d).


Instalação
-------------------------

Ao instalar, para reinicializar e alimentar o banco de dados com dados de exemplo,
lembre-se de executar no diretório da aplicação:

- rake db:setup



**Todos os testes automatizados podem ser executados, como no exemplo:**

```html
- Dentro da pasta da aplicação:

$ rspec


```