class AddVirtualCodeToPosts < ActiveRecord::Migration
  def change
    add_index :posts, :virtual_code
  end
end
