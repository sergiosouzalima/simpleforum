class AddCodeLevelToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :code_level, :integer, default: 0
    add_index :posts, :code_level
  end
end
