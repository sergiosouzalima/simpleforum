class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :code
      t.string :virtual_code
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :posts, [:code, :user_id], unique: true
  end
end
