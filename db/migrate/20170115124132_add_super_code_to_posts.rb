class AddSuperCodeToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :super_code, :string
    add_index :posts, :super_code
  end
end
