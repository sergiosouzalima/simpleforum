# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
admin_user = CreateAdminService.new.call
puts 'CREATED ADMIN USER: ' << admin_user.email

vip_user = User.find_or_create_by!(email: 'bob@simpleforum.com') do |user|
  user.name = 'Bob'
  user.email = 'bob@simpleforum.com'
  user.password = 'password'
  user.password_confirmation = 'password'
  user.vip!
end
puts 'CREATED VIP USER: ' << vip_user.email

user_user1 = User.find_or_create_by!(email: 'john@simpleforum.com') do |user|
  user.name = 'John'
  user.email = 'john@simpleforum.com'
  user.password = 'password'
  user.password_confirmation = 'password'
  user.user!
end
puts 'CREATED USER: ' << user_user1.email

user_user2 = User.find_or_create_by!(email: 'joe@simpleforum.com') do |user|
  user.name = 'Joe'
  user.email = 'joe@simpleforum.com'
  user.password = 'password'
  user.password_confirmation = 'password'
  user.user!
end
puts 'CREATED USER: ' << user_user2.email

user_user3 = User.find_or_create_by!(email: 'ajfprates@gmail.com') do |user|
  user.name = 'Santiago Martinez Carrilho Junior'
  user.email = 'ajfprates@gmail.com'
  user.password = 'password'
  user.password_confirmation = 'password'
  user.user!
end
puts 'CREATED USER: ' << user_user3.email

