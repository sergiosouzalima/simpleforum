Rails.application.routes.draw do
  resources :posts

  get '/threads/:id', to: 'posts#new', as: 'thread'

  namespace :admin do
    resources :users
    root to: "users#index"
  end
  root to: 'posts#index'
  devise_for :users
  resources :users
end
